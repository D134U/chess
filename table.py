from point import Point


class Table():
    table = {(x, y) for y in range(1, 9) for x in range(1, 9)}
    table_positions = {Point(x, y) for x, y in table}

    def __init__(self):
        self.positions = {(x, y) for y in range(1, 9) for x in range(1, 9)}
        self.black = []
        self.white = []
        self.table = {(x, y) for y in range(1, 9) for x in range(1, 9)}
        self.table_positions = {Point(x, y) for x, y in self.table}

    def _make_relation(self):
        """подготовка связей между нодами графа"""
        self.related_points = {}
        ways = {(1, 0),
                (0, 1),
                (-1, 0),
                (0, -1),
                (-1, -1),
                (1, 1),
                (1, -1),
                (-1, 1)}
        for point in self.table_positions:
            for way in ways:
                x = point.x + way[0]
                y = point.y + way[1]
                endpoint = Point(x, y)
                if endpoint.isExisted():
                    if self.related_points.get(point):
                        self.related_points[point].append(endpoint)
                    else:
                        self.related_points[point] = [endpoint]

    def prepare_figure(self):
        """подготовка фигур на доске"""
        pass


