class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def isExisted(self):
        if (self.x <= 8 and self.x >= 1) and (self.y <= 8 and self.y >= 1):
            return True
        else:
            return False

    def __repr__(self):
        return 'Point({self.x}, {self.y})'.format(self=self)

